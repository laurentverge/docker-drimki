# Drimki

## Contexte projet

Le contexte technique (version de PHP, etc) est listé dans les fichiers _infrastructure/config.stage.json_ et _infrastructure/config.prod.json_

## Installation

### Pré-requis

- Docker
- Docker compose
- GIT

### Récupération des sources

Cloner le projet Drimki Website et se placer sur la branche `develop` :

```bash
git clone git@bitbucket.org:digitregroup/drimki-website.git
cd drimki-website
git checkout develop
```

Cloner le projet Grav CMS :

```bash
git clone git@github.com:digitregroup/grav.git
```

Supprimer le dossier `user` du CMS afin de laisser la place au contenu des différents sites.

:information_source: Cette suppression a des répercussions sur le projet. Il peut être prudent de créer une branche dérivée de `develop` avant de supprimer ce dossier.

### Création des images et des conteneurs

Copier le fichier `.env.template` et le nommer `.env`.

Remplacer le contenu de ce fichier par les identifiants et chemins correspondant à votre environnement local.

Copier le fichier `.git-credentials.template` et le nommer `.git-credentials`.

Remplacer le contenu de ce fichier par vos identifiants de connexion à BitBucket. Remarque : le mot de passe doit être _url encodé_ (via https://www.urlencoder.org/ par exemple).

Passer ensuite la commande :

```bash
sudo docker-compose build --force-rm
```

### Installation des sources PHP

Avant de procéder à l'installation associez, si ce n'est pas déjà fait, votre clé SSH publique à votre profil sur **GitHub** et **BitBucket**.

Puis, passer la commande :

```bash
sudo docker-compose run --rm fpm php bin/grav install
```

:information_source: Certains plugins étant récupérés via HTTPS, vous aurez à saisir plusieurs fois vos identifiants BitBucket.

### Compilation des sources front

Passer les commandes :

```bash
sudo docker-compose run --rm front yarn install
```

```bash
sudo docker-compose run --rm front yarn css:compile
```

### Données

#### Polygone

Récupérer la liste des villes : [assets DigitRE - Drimki](s3://assets.digitregroup.io/drimki/website/)

Déposer le contenu de l'archive dans le répertoire du plugin SEOFactory : `plugins/seofactory/data/polygones/`

## Cycle de vie du projet

### Lancement des conteneurs

Passer la commande :

```bash
sudo docker-compose up --build --detach --no-recreate
```

Le site est maintenant accessible via `localhost:8080`

### Compilation auto des sources front

Afin d'obtenir une compilation automatique des sources lorsqu'elles sont modifiées, passer la commande suivante :

```bash
sudo docker-compose run -d --rm front yarn css:watch
```

### Arrêt des conteneurs

Passer la commande :

```bash
sudo docker-compose down
```

## Architecture

### Configuration

Voici où trouver les fichiers de configuration principaux :

- global : `drimki-website/config/system.yaml`
- local : `drimki-website/localhost/config/system.yaml`
- stage : `drimki-website/stage.drimki.fr/config/system.yaml`

Les fichiers liés au déploiement de l'application sont les suivants :

- CircleCI : `drimki-website/.circleci/config.yml`
- Elastic Beanstalk : `drimki-website/._ebextensions/*.config`

## Environnements 

| local                       | stage | production                                                   |
| --------------------------- | ----- | ------------------------------------------------------------ |
| [URL front](localhost:8080) |       | [URL front](https://www.drimki.fr)                           |
|                             |       | [pipeline déploiement](https://app.circleci.com/pipelines/bitbucket/digitregroup/drimki-website) |
|                             |       |                                                              |