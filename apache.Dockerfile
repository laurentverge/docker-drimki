# drimki project - apache
FROM php:7.3-apache
MAINTAINER DigitRE

# variables
ARG UID=1000
ARG GID=1000

# system dependencies
RUN apt-get update -y
RUN apt-get install -y \
    curl \
    git \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libxml2-dev \
    libzip-dev \
    ssh \
    unzip \
    zip \
    --no-install-recommends

# php extensions
RUN docker-php-ext-install -j$(nproc) \
    dom \
    gd \
    zip

# clear package lists
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

# apache configuration
RUN a2enmod rewrite