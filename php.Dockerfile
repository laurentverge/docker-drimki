# drimki project - php
FROM php:7.2-fpm
MAINTAINER DigitRE

# variables
ARG UID=1000
ARG GID=1000

# system dependencies
RUN apt-get update -y
RUN apt-get install -y \
    curl \
    git \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libxml2-dev \
    libzip-dev \
    ssh \
    unzip \
    zip \
    --no-install-recommends

# php extensions
RUN docker-php-ext-install -j$(nproc) \
    dom \
    gd \
    opcache \
    zip

# clear package lists
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

# git credentials
COPY ./php-bitbucket-config /root/.ssh/config
COPY ./.git-credentials /root/credentials/.git-credentials

RUN git config --global credential.helper "store --file /root/credentials/.git-credentials"
